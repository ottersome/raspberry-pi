import gyroq 
import time

sensors = gyroq.gy801()
gyro = sensors.gyro
while 1:
    try:
        # if run directly we'll just create an instance of the class and output 
        # the current readings
        
        gyro.getXangle()
        gyro.getYangle()
        gyro.getZangle()
        
        print ("Gyro: ")
        print ("Xangle = %.3f deg" % ( gyro.getXangle() ))
        print ("Yangle = %.3f deg" % ( gyro.getYangle() ))
        print ("Zangle = %.3f deg" % ( gyro.getZangle() ))

        time.sleep(1)
            
    except KeyboardInterrupt:
        print("Cleanup")

