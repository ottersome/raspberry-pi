import  accq
import time
try:
    sensors = accq.gy801()
    adxl345 = sensors.accel
    adxl345.setOffset()

except KeyboardInterrupt:
    print("Cleanup")

while(1):
    try:

        adxl345.getX()
        adxl345.getY()
        adxl345.getZ()

        adxl345.getXg()
        adxl345.getYg()
        adxl345.getZg()

        print ("ACC: ")
    #   print ("x = %.3f m/s2" % ( adxl345.X ))
    #   print ("y = %.3f m/s2" % ( adxl345.Y ))
    #   print ("z = %.3f m/s2" % ( adxl345.Z ))
        print ("x = %.3fG" % ( adxl345.Xg ))
        print ("y = %.3fG" % ( adxl345.Yg ))
        print ("z = %.3fG" % ( adxl345.Zg ))
        print ("Raw Info:")
        print ("x = %.3f" % ( adxl345.Xraw ))
        print ("y = %.3f" % ( adxl345.Yraw ))
        print ("z = %.3f" % ( adxl345.Zraw ))
        print ("Pitch And Roll")
        print ("Pitch = %.3f" % (adxl345.getPitch()))
        print ("Roll = %.3f" % ( adxl345.getRoll() ))
    #    print ("pitch = %.3f" % ( adxl345.getPitch() ))
    #    print ("roll = %.3f" % ( adxl345.getRoll() ))i
        time.sleep(1)
            
    except KeyboardInterrupt:
        print("Cleanup")

