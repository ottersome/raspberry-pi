import accq
import time


try:
    sensors = accq.gy801()
    adxl345 = sensors.accel
    adxl345.setOffset()

except KeyboardInterrupt:
    print("Cleanup")

while(1):
    try:

        adxl345.getX()
        adxl345.getY()
        adxl345.getZ()

        adxl345.getXg()
        adxl345.getYg()
        adxl345.getZg()


        print ("Dist: ")
        print ("x = %.3f m/s2" % ( adxl345.X ))
        print ("y = %.3f m/s2" % ( adxl345.Y ))
        print ("z = %.3f m/s2" % ( adxl345.Z ))
        print ("x = %.3fcm" % ( adxl345.getXdist() *100))
        print ("y = %.3fcm" % ( adxl345.getYdist() *100))
        print ("y = %.3fcm" % ( adxl345.getZdist() *100))
        #print ("z = %.3cmG" % ( adxl345.Zg ))
        time.sleep(0.1)
            
    except KeyboardInterrupt:
        print("Cleanup")

