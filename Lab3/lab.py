bus = smbus.SMBus(1);

EARTH_GRAVITY_MS2 = 9.80665

ADXL345_ADDRESS = 0x53
ADXL345_BW_RATE = 0x2C
ADXL345_POWER_CTL = 0x2D
ADXL345_DATA_FORMAT = 0x31
ADXL345_DATAX0 = 0x32
ADXL345_DATAX1 = 0x33
ADXL345_DATAY0 = 0x34
ADXL345_DATAY1 = 0x35
ADXL345_DATAZ0 = 0x36
ADXL345_DATAZ1 = 0x37


ADXL345_SCALE_MULTIPLIER=0.00390625
ADXL345_BW_RATE_100HZ = 0x0A
ADXL345_MEASURE = 0x08


def write_byte(self,adr,value):
    bus.write_byte_data(self.ADDRESS,adr,value)
def read_byte(self,adr):
    return bus.read_byte_data(self.ADDRESS,adr)
def read_word(self,adr,rf=1):
    if(rf == 1):
        low = self.read_byte(adr)
        high = self.read_byte(adr+1)
    else:
        high = self.read_byte(adr)
        low = self.read_byte(adr+1)
    #Combine High and Low
    val = (high<<8)+low
    return val

def read_word_2c(self,adr,rf=1):
    val = self.read_word(adr,rf)
    if(val & (1<<16-1)):
        return val - (1<<16)
    else:
        return val

def getRawX(self):
    self.Xraw = self.read_word_2c(ADXL345_DATAX0)
    return self.Xraw

def getRawY(self):
    self.Yraw = self.read_word_2c(ADXL345_DATAY0)
    return self.Yraw
def  getRawZ(self):
    self.Zraw = self.read_word_2c(ADXL345_DATAZ0)
    return self.Zraw

self.write_byte(ADXL345_BW_RATE,ADXL345_BW_RATE_100HZ)
self.write_byte(ADXL345_POWER_CTL,ADXL345_MEASURE)
self.write_bute(ADXL345_DATA_FORMAT,self.df_value)


