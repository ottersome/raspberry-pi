import smbus
import time
import math
from acc import IMU, ADXL345
from acc import gy801 as gy801_acc
from math import *

bus = smbus.SMBus(1);            # 0 for R-Pi Rev. 1, 1 for Rev. 2

L3G4200D_ADDRESS        =    0x69
L3G4200D_CTRL_REG1      =    0x20
L3G4200D_CTRL_REG4      =    0x23
L3G4200D_OUT_X_L        =    0x28
L3G4200D_OUT_X_H        =    0x29
L3G4200D_OUT_Y_L        =    0x2A
L3G4200D_OUT_Y_H        =    0x2B
L3G4200D_OUT_Z_L        =    0x2C
L3G4200D_OUT_Z_H        =    0x2D

class IMU(object):

    def write_byte(self,adr, value):
        bus.write_byte_data(self.ADDRESS, adr, value)
    
    def read_byte(self,adr):
        return bus.read_byte_data(self.ADDRESS, adr)

    def read_word(self,adr,rf=1):
        # rf=1 Little Endian Format, rf=0 Big Endian Format
        if (rf == 1):
            low = self.read_byte(adr)
            high = self.read_byte(adr+1)
        else:
            high = self.read_byte(adr)
            low = self.read_byte(adr+1)
        val = (high << 8) + low
        return val

    def read_word_2c(self,adr,rf=1):
        val = self.read_word(adr,rf)
        if(val & (1 << 16 - 1)):
            return val - (1<<16)
        else:
            return val

class gy801(object):
    def __init__(self) :
        self.gyro = L3G4200D()


class L3G4200D(IMU):
    
    ADDRESS = L3G4200D_ADDRESS

    def __init__(self) :
        #Class Properties
        self.Xraw = 0.0
        self.Yraw = 0.0
        self.Zraw = 0.0
        self.X = 0.0
        self.Y = 0.0
        self.Z = 0.0
        self.Xangle = 0.0
        self.Yangle = 0.0
        self.Zangle = 0.0
        self.t0x = None
        self.t0y = None
        self.t0z = None

        # set value
        self.gain_std = 0.00875    # dps/digit
        
        self.write_byte(L3G4200D_CTRL_REG1, 0x0F)
        self.write_byte(L3G4200D_CTRL_REG4, 0x80)

        self.setCalibration()

    def setCalibration(self) :
        gyr_r = self.read_byte(L3G4200D_CTRL_REG4)
        
        self.gain = 2 ** ( gyr_r & 48 >> 4) * self.gain_std

    def getRawX(self):
        self.Xraw = self.read_word_2c(L3G4200D_OUT_X_L)
        return self.Xraw

    def getRawY(self):
        self.Yraw = self.read_word_2c(L3G4200D_OUT_Y_L)
        return self.Yraw

    def getRawZ(self):
        self.Zraw = self.read_word_2c(L3G4200D_OUT_Z_L)
        return self.Zraw

    def getX(self,plf = 1.0):
        self.X = ( self.getRawX() * self.gain ) * plf + (1.0 - plf) * self.X
        return self.X

    def getY(self,plf = 1.0):
        self.Y = ( self.getRawY() * self.gain ) * plf + (1.0 - plf) * self.Y
        return self.Y

    def getZ(self,plf = 1.0):
        self.Z = ( self.getRawZ() * self.gain ) * plf + (1.0 - plf) * self.Z
        return self.Z
    
    def getXangle(self,plf = 1.0) :
        if self.t0x is None : self.t0x = time.time()
        t1x = time.time()
        LP = t1x - self.t0x
        self.t0x = t1x
        self.Xangle = self.getX(plf) * LP
        return self.Xangle
    
    def getYangle(self,plf = 1.0) :
        if self.t0y is None : self.t0y = time.time()
        t1y = time.time()
        LP = t1y - self.t0y
        self.t0y = t1y
        self.Yangle = self.getY(plf) * LP
        return self.Yangle
    
    def getZangle(self,plf = 1.0) :
        if self.t0z is None : self.t0z = time.time()
        t1z = time.time()
        LP = t1z - self.t0z
        self.t0z = t1z
        self.Zangle = self.getZ(plf) * LP
        return self.Zangle

def normal(x,y,z):
    return sqrt(x**2 + y**2 + z**2)


acttime  = time.time()
angle = 0
pitch = 0
roll = 0

while(1):
    try:
        #ACcel
        sensorsaccel = gy801_acc()
        adxl345 = sensorsaccel.accel
        adxl345.getX()
        adxl345.getY()
        adxl345.getZ()
        # if run directly we'll just create an instance of the class and output 
        # the current readings
        
        #Gyro
        sensors = gy801()

        gyro = sensors.gyro
        
        gyro.getXangle()
        gyro.getYangle()
        gyro.getZangle()
        
        print ("Quiz 3: ")
        #print ("Xangle = %.3f deg" % ( gyro.getXangle() ))
        #print ("Yangle = %.3f deg" % ( gyro.getYangle() ))
        #print ("Zangle = %.3f deg" % ( gyro.getZangle() ))
        dt = time.time() - acttime
        print("Dt is : %.3f"%dt)
        gyroData = normal(gyro.getX(),gyro.getY(),gyro.getZ())
        accData = normal(adxl345.X,adxl345.Y,adxl345.Z)

        angle = 0.98 * (angle + gyroData * dt) + 0.02*accData

        rollAcc = adxl345.Yg / adxl345.Zg
        pitchAcc = -adxl345.Xg / math.sqrt(adxl345.Yg**2 + adxl345.Zg**2)

        pitch = (pitch + gyro.getXangle())*0.98 + pitchAcc * 0.02
        roll = (roll + gyro.getYangle()) * 0.98 + rollAcc*0.02

        print("Angle %.3f : "%angle)
        print("Roll %.3f : "%roll)
        print("Pitch %.3f : "%pitch)

        acttime = time.time()
        time.sleep(1)
        
    except KeyboardInterrupt:
        print("Cleanup")
        
