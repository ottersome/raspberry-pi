from __future__ import print_function
import sys
import cv2
from imutils.video.pivideostream import PiVideoStream
from imutils.video import FPS

import imutils
import time
import cv2


class Camera(object):
    def __init__(self):
        self.cascPath = "model/haarcascade_frontalface_default.xml"
        self.faceCascade = cv2.CascadeClassifier(self.cascPath)
        if cv2.__version__.startswith('2'):
            PROP_FRAME_WIDTH = cv2.cv.CV_CAP_PROP_FRAME_WIDTH
            PROP_FRAME_HEIGHT = cv2.cv.CV_CAP_PROP_FRAME_HEIGHT
        elif cv2.__version__.startswith('3'):
            PROP_FRAME_WIDTH = cv2.CAP_PROP_FRAME_WIDTH
            PROP_FRAME_HEIGHT = cv2.CAP_PROP_FRAME_HEIGHT
        #self.video = PiVideoStream().start()
        self.vs = cv2.VideoCapture(0)
        self.vs.set(PROP_FRAME_WIDTH, 320)
        self.vs.set(PROP_FRAME_HEIGHT, 240)
        time.sleep(2.0)
        #self.video = cv2.VideoCapture(1)
        #self.video.set(PROP_FRAME_WIDTH, 640)
        #self.video.set(PROP_FRAME_HEIGHT, 480)
        #self.video.set(PROP_FRAME_WIDTH, 320)
        #self.video.set(PROP_FRAME_HEIGHT, 240)

        print("DOne with constructor")

    
    def __del__(self):
        vs.release()
        #self.video.release()
    
    def get_frame(self):
        #success, image = self.video.read()
        #image = self.video.read()
        ret, frame = self.vs.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # https://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html
        faces = self.faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30)
        )
        print ("Found {0} faces!".format(len(faces)))
        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        ##image = imutils.resize(image,width=480)
        ret, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tostring()
