import RPi.GPIO as GPIO
import dht11
import time
import datetime

# initialize GPIO
GPIO.setwarnings(True)
GPIO.setmode(GPIO.BCM)

# read data using pin 
instance = dht11.DHT11(pin=4)
#Limit Temperature
tempo = 25
LED_PIN = 18
GPIO.setup(LED_PIN,GPIO.OUT) 
try:
    while False:
        result = instance.read()
        if result.is_valid():
            print("Last valid input: " + str(datetime.datetime.now()))

            print("Temperature: %-3.1f C" % result.temperature)
            if(result.temperature > tempo):
                print("Above limit of ")
                GPIO.output(LED_PIN,True) 
            else:
                GPIO.output(LED_PIN,False) 
            print("Humidity: %-3.1f %%" % result.humidity)

        time.sleep(6)

    # Quiz 2
    ## We already have the temperature

except KeyboardInterrupt:
    print("Cleanup")

finally:
    GPIO.cleanup()
