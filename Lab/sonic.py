import RPi.GPIO as GPIO
import time
import dht11
import datetime

# initialize GPIO
GPIO.setwarnings(True)

# read data using pin 
#Limit Temperature
#LED_PIN = 18
#GPIO.setup(LED_PIN,GPIO.OUT) 

v= 343
TRIGGER_PIN = 16
ECHO_PIN = 18
GPIO.setmode(GPIO.BOARD)
GPIO.setup(TRIGGER_PIN,GPIO.OUT)
GPIO.setup(ECHO_PIN,GPIO.IN)
LED_PING = 12
GPIO.setup(LED_PING,GPIO.OUT)

instance = dht11.DHT11(pin=7)
def measure():
    state = True
    while True:
        GPIO.output(TRIGGER_PIN,GPIO.HIGH)
        time.sleep(0.00001)
        GPIO.output(TRIGGER_PIN,GPIO.LOW)
        pulse_start = time.time()#I think thi is just default value
        while GPIO.input(ECHO_PIN) == GPIO.LOW:
                pulse_start = time.time()
        while GPIO.input(ECHO_PIN) == GPIO.HIGH:
                pulse_end = time.time()
        t = pulse_end - pulse_start
        print("T is  %d"%t)
        d=0
        #Get Temperaturee
        temp = 0
        result = instance.read()
        
        if result.is_valid():
            print("Last valid input: " + str(datetime.datetime.now()))
            print("Temperature: %-3.1f C" % result.temperature)
            print("Humidity: %-3.1f %%" % result.humidity)

            time.sleep(1)
            c = 331.3 + 0.606*result.temperature
            d = t*c
            d = d/2
            d = d*100
            print("Distance is : %f\n"%d)
            if d > 100:
                state = False
                GPIO.output(LED_PING,state)
            elif(d> 30 and d <100):
                state = not state
                GPIO.output(LED_PING,state)
                time.sleep(1.0)
            else:
                state = not state
                GPIO.output(LED_PING,state)
                time.sleep(0.3)

    return 100

measure()
GPIO.cleanup()
