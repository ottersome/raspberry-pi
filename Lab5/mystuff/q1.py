import time
import picamera
import datetime
import schedule 
import time

def pew():
    print("Starting Taking IMages : ")
    with picamera.PiCamera() as camera:
        camera.start_preview()
        try:
            #for i, filename in enumerate(camera.capture_continuous('image{counter:02d}.jpg')):
            for i, filename in enumerate(camera.capture_continuous('image{timestamp:%Y%m%d_%H%M}.jpg')):
                print(filename)
                time.sleep(1)
                if i == 2:
                    break
        finally:
            camera.stop_preview()

print("Scheduling...")
currentDT = datetime.datetime.now()
nextThing = currentDT + datetime.timedelta(0,5,0,0,minutes=1)
#schedule.every().day.at(currentDT.strftime("%H:%M")).do(pew)
schedule.every(10).seconds.do(pew) 
print("Scheduled. Now Waiting... until  :%s"%currentDT.strftime("%H:%M:%S"))
while True:
    schedule.run_pending()
    time.sleep(1)
