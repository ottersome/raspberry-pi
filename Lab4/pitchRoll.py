#import accq
import magq
import time
pitch = 0

try:
    sensors = magq.gy801()
    adxl345 = sensors.accel
    #adxl345.setOffset()
    gyro = sensors.gyro

    fusepitch =0
    fuseroll =0
    while(1):
        
        gyro.getXangle()
        gyro.getYangle()
        gyro.getZangle()

        adxl345.getX()
        adxl345.getY()
        adxl345.getZ()

        print ("pitch = %.3f" % ( adxl345.getPitch() *180/3.1415))
        print ("roll = %.3f" % ( adxl345.getRoll() *180/3.1415))

        fusepitch = (fusepitch + gyro.getXangle())*0.98 + adxl345.getPitch() * 0.02
        fuseroll = (fuseroll + gyro.getYangle()) * 0.98 + adxl345.getRoll()*0.02
        print ("\tfusepitch = %.3f" % (fusepitch*180/3.1415))
        print ("\tfuseroll = %.3f" % (fuseroll*180/3.1415))
        time.sleep(1)

except KeyboardInterrupt:
    print("Cleanup")
