import magq
import time
from math import *

try:
    sensors = magq.gy801()
    barometer = sensors.baro
    compass = sensors.compass
    adxl345 = sensors.accel
    gyro = sensors.gyro
    pitch = 0
    roll = 0
    # if run directly we'll just create an instance of the class and output 
    # the current readings
    while 1:
        tempC = barometer.getTempC()
        tempF = barometer.getTempF()
        press = barometer.getPress()
        altitude = barometer.getAltitude()

        magx = compass.getX()
        magy = compass.getY()
        magz = compass.getZ()
        #--------------------------------------------------
        # Get ROll and Pitch
        # calculate pitch, roll, tilt. Nice they already do this for us. LUL nup. this is the basic way
        aX = adxl345.getX()
        aY = adxl345.getY()
        aZ = adxl345.getZ()
        
        acc_roll = atan2(aY,aZ) 
        acc_pitch = -1 * atan2(-aX,sqrt(aY*aY+aZ*aZ)) 

        #angle = 0.98 * (angle + gyroData * dt) + 0.02*accData We dont actually need this

        pitch = (pitch + gyro.getXangle())*0.98 + acc_pitch * 0.02
        roll = (roll + gyro.getYangle()) * 0.98 + acc_roll*0.02
        tilt = adxl345.getTilt()

        # --------------------------------------------------
        # Heading
        bearing1  = degrees(atan2(magy, magx))

        if (bearing1 < 0):
            bearing1 += 360
        if (bearing1 > 360):
            bearing1 -= 360
        bearing1 = bearing1 + compass.angle_offset
        
        # Tilt compensate
        compx = magx * cos(pitch) + magz * sin(pitch)
        compy = magx * sin(roll) * sin(pitch) \
                + magy * cos(roll) \
                - magz * sin(roll) * cos(pitch)

        bearing2  = degrees(atan2(compy, compx))
        if (bearing2 < 0):
            bearing2 += 360
        if (bearing2 > 360):
            bearing2 -= 360
        bearing2 = bearing2 + compass.angle_offset
        # --------------------------------------------------

        print("Roll:%.3f,Pitch:%.3f,Tilt:%.3f,Headin:%.3f,Altitude:%.3f"%(roll,pitch,tilt,bearing2,altitude))
    
        #print ("Barometer:" )
        #print ("   Temp: %f C (%f F)" %(tempC,tempF))
        #print ("   Press: %f (hPa)" %(press))
        #print ("   Altitude: %f m s.l.m" %(altitude))
        time.sleep(1)
        
except KeyboardInterrupt:
    print("Cleanup")
