import magq
import time
from math import *

try:
    sensors = magq.gy801()
    compass = sensors.compass
    gyro = sensors.gyro
    compass.calibrateCompass()
    adxl345 = sensors.accel

    #Inital Value
    pitch = 0
    roll = 0

    while True:
        magx = compass.getX()
        magy = compass.getY()
        magz = compass.getZ()
    
        # --------------------------------------------------
        # calculate pitch, roll, tilt. Nice they already do this for us. LUL nup. this is the basic way
        aX = adxl345.getX()
        aY = adxl345.getY()
        aZ = adxl345.getZ()
        
        acc_roll = atan2(aY,aZ) 
        acc_pitch = -1 * atan2(-aX,sqrt(aY*aY+aZ*aZ)) 

        #angle = 0.98 * (angle + gyroData * dt) + 0.02*accData We dont actually need this

        pitch = (pitch + gyro.getXangle())*0.98 + acc_pitch * 0.02
        roll = (roll + gyro.getYangle()) * 0.98 + acc_roll*0.02
        # --------------------------------------------------
        #Hmmmm looks like we have to do something here
        
        # --------------------------------------------------
        # Heading
        bearing1  = degrees(atan2(magy, magx))

        if (bearing1 < 0):
            bearing1 += 360
        if (bearing1 > 360):
            bearing1 -= 360
        bearing1 = bearing1 + compass.angle_offset
        
        # Tilt compensate
        compx = magx * cos(pitch) + magz * sin(pitch)
        compy = magx * sin(roll) * sin(pitch) \
                + magy * cos(roll) \
                - magz * sin(roll) * cos(pitch)

        bearing2  = degrees(atan2(compy, compx))
        if (bearing2 < 0):
            bearing2 += 360
        if (bearing2 > 360):
            bearing2 -= 360
        bearing2 = bearing2 + compass.angle_offset
        # --------------------------------------------------

        
#        print ("Compass: " )
#        print ("X = %d ," % ( magx )),
#        print ("Y = %d ," % ( magy )),
#        print ("Z = %d (gauss)" % ( magz ))
#        print ("tiltX = %.3f ," % ( compx )),
#        print ("tiltY = %.3f ," % ( compy )),
       
#        print ("Angle offset = %.3f deg" % ( compass.angle_offset ))
        print ("Original Heading = %.3f deg, " % ( bearing1 )), 
        print ("Tilt Heading = %.3f deg, " % ( bearing2 ))
        #time.sleep(1)

        
except KeyboardInterrupt:
    print("Cleanup")

