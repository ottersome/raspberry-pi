import smbus
import math
import time
from math import *

bus = smbus.SMBus(1);            # 0 for R-Pi Rev. 1, 1 for Rev. 2

EARTH_GRAVITY_MS2    = 9.80665 # m/s2

ADXL345_ADDRESS    =    0x53

ADXL345_BW_RATE          =    0x2C 
ADXL345_POWER_CTL        =    0x2D 
ADXL345_DATA_FORMAT      =    0x31 
ADXL345_DATAX0           =    0x32
ADXL345_DATAY0           =    0x34
ADXL345_DATAZ0           =    0x36
ADXL345_SCALE_MULTIPLIER = 0.00390625    # G/LSP # TODO I naeed ot make sure of this value
ADXL345_BW_RATE_100HZ    = 0x0A # 0000 1100 THis is jsut the default value, meaning Data rate and power control mode
ADXL345_MEASURE          = 0x08

class IMU(object):

    def write_byte(self,adr, value):
        bus.write_byte_data(self.ADDRESS, adr, value)
    
    def read_byte(self,adr):
        return bus.read_byte_data(self.ADDRESS, adr)

    def read_word(self,adr,rf=1):
        # rf=1 Little Endian Format, rf=0 Big Endian Format
        if (rf == 1):
            low = self.read_byte(adr)
            high = self.read_byte(adr+1)
        else:
            high = self.read_byte(adr)
            low = self.read_byte(adr+1)
        val = (high << 8) + low
        return val

    def read_word_2c(self,adr,rf=1):
        val = self.read_word(adr,rf)
        if(val & (1 << 16 - 1)):
            return val - (1<<16)
        else:
            return val

class gy801(object):
    def __init__(self) :
        self.accel = ADXL345()

class ADXL345(IMU):
    
    ADDRESS = ADXL345_ADDRESS
    
    def __init__(self) :
        #Class Properties
        self.Xoffset = 0.0
        self.Yoffset = 0.0
        self.Zoffset = 0.0
        self.Xraw = 0.0
        self.Yraw = 0.0
        self.Zraw = 0.0
        self.Xg = 0.0
        self.Yg = 0.0
        self.Zg = 0.0
        self.X = 0.0
        self.Y = 0.0
        self.Z = 0.0
        self.t0x = None
        self.t0y = None
        self.t0z = None
        self.Xdist = 0
        self.Ydist = 0
        self.Zdist = 0
        self.pitch = 0
        self.tilt = 0
        self.roll = 0
        self.df_value = 0b00001000    # Self test disabled, 4-wire interface
                                # Full resolution, Range = +/-2g
        self.Xcalibr = ADXL345_SCALE_MULTIPLIER
        self.Ycalibr = ADXL345_SCALE_MULTIPLIER
        self.Zcalibr = ADXL345_SCALE_MULTIPLIER

        self.write_byte(ADXL345_BW_RATE, ADXL345_BW_RATE_100HZ)    # Normal mode, Output data rate = 100 Hz
        self.write_byte(ADXL345_POWER_CTL, ADXL345_MEASURE)    # Auto Sleep disable
        self.write_byte(ADXL345_DATA_FORMAT, self.df_value)    
    
    # RAW readings in LPS
    def getRawX(self) :
        self.Xraw = self.read_word_2c(ADXL345_DATAX0)
        return self.Xraw

    def getRawY(self) :
        self.Yraw = self.read_word_2c(ADXL345_DATAY0)
        return self.Yraw
    
    def getRawZ(self) :
        self.Zraw = self.read_word_2c(ADXL345_DATAZ0)
        return self.Zraw

    # G related readings in g
    def getXg(self,plf = 1.0) :
        self.Xg = (self.getRawX() * self.Xcalibr + self.Xoffset) * plf + (1.0 - plf) * self.Xg
        return self.Xg

    def getYg(self,plf = 1.0) :
        self.Yg = (self.getRawY() * self.Ycalibr + self.Yoffset) * plf + (1.0 - plf) * self.Yg
        return self.Yg

    def getZg(self,plf = 1.0) :
        self.Zg = (self.getRawZ() * self.Zcalibr + self.Zoffset) * plf + (1.0 - plf) * self.Zg
        return self.Zg
    
    # Absolute reading in m/s2
    def getX(self,plf = 1.0) :
        self.X = self.getXg(plf) * EARTH_GRAVITY_MS2
        return self.X
    
    def getY(self,plf = 1.0) :
        self.Y = self.getYg(plf) * EARTH_GRAVITY_MS2
        return self.Y
    
    def getZ(self,plf = 1.0) :
        self.Z = self.getZg(plf) * EARTH_GRAVITY_MS2
        return self.Z

    #Get X Distance through Integration From Velocity
    def getXdist(self,plf = 1.0) :
        if self.t0x is None : self.t0x = time.time()
        t1x = time.time()
        LP = t1x - self.t0x
        self.t0x = t1x
        self.Xdist = self.Xdist + 0.5*(self.getX(plf) * LP*LP)
        return self.Xdist

    #Get Y Distance through Integration From Velocity
    def getYdist(self,plf = 1.0) :
        if self.t0y is None : self.t0y = time.time()
        t1y = time.time()
        LP = t1y - self.t0y
        self.t0y = t1y
        self.Ydist = self.Ydist + 0.5*(self.getY(plf) * LP*LP)
        return self.Ydist

    #Get Z Distance through Integration From Velocity
    def getZdist(self,plf = 1.0) :
        if self.t0z is None : self.t0z = time.time()
        t1z = time.time()
        LP = t1z - self.t0z
        self.t0z = t1z
        self.Zdist = self.Zdist + 0.5*(self.getZ(plf) * LP*LP)
        return self.Zdist

    def getPitch(self) :
        aX = self.getXg()
        aY = self.getYg()
        aZ = self.getZg()
        #self.pitch = math.atan()
        self.pitch = -1*math.atan2(-aX,math.sqrt(aY**2 + aZ**2))
        return self.pitch 
    def getTilt(self):
        # atan2()aZ,sqrt(ax^2+ay^2+ax^2)
        aX = self.getXg()
        aY = self.getYg()
        aZ = self.getZg()
        self.tilt = math.atan2(aZ,math.sqrt(aX**2 + aY**2 + aZ**2))
        return self.tilt

    def getRoll(self) :
        aX = self.getXg()
        aY = self.getYg()
        aZ = self.getZg()
        self.roll = math.atan2(aY,aZ)
        return self.roll

    def setOffset(self):
        #Get about ten samples for 100Mhz
        print("Setting offset...")
        xo = 0
        yo = 0
        zo = 0
        for i in range(9):
            xo = xo + self.getRawX()
            yo = yo + self.getRawY()
            zo = zo + self.getRawZ()
            print("Sample X:%.3f,Y:%.3f,Z:%.3f"%(self.getRawX(),self.getRawY(),self.getRawZ()))

        #Average the up 
        #self.Xoffset = -(xo/10 * self.Xcalibr)
        #self.Yoffset = -(yo/10 * self.Ycalibr)
        #self.Zoffset = -(zo/10 * self.Zcalibr)
        print("Offsets: X:%.3f,Y:%.3f,Z:%.3f"% (self.Xoffset,self.Yoffset,self.Zoffset))

